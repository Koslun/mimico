package game.party;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;

public class PartyStarterActivity extends Activity implements OnClickListener {
	Button quick, custom, settings;
	ImageView im;
	Bundle b;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        ViewGroup layout = (ViewGroup) findViewById(R.id.main);
        setLayoutAnimation(layout, this);
        
        im = (ImageView) findViewById(R.id.logo);
        quick = (Button) findViewById(R.id.button1);
        custom = (Button) findViewById(R.id.button2);
        settings = (Button) findViewById(R.id.button3);
        quick.setOnClickListener(this);
        custom.setOnClickListener(this);
        settings.setOnClickListener(this);
        
        
        /*MyApp appState = ((MyApp)getApplicationContext()); 
        appState.setState("pelle");
        String state = appState.getState(); 
        quick.setText(state);
*/
        
    }

	private void setLayoutAnimation(ViewGroup panel, Context ctx) {
		LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(ctx, R.anim.layout_anim_controller);
		panel.setLayoutAnimation(controller);
	}

	
	public void onClick(View v) {
		im.setVisibility(ImageView.INVISIBLE);
		if (v.getId() == R.id.button1){
			Intent newqui = new Intent(PartyStarterActivity.this, SensorActivity.class); 
			b = new Bundle();
			b.putInt("movementTime", 5);
			newqui.putExtras(b);
			startActivityForResult(newqui, 0); 
		}
		if (v.getId() == R.id.button2){
			Intent newcus = new Intent(PartyStarterActivity.this, SensorActivity.class);
			b = getIntent().getExtras();
			newcus.putExtras(b);
			startActivityForResult(newcus, 0); 
		}
		if (v.getId() == R.id.button3){
			//Intent startNewActivityOpen = new Intent(PartyStarterActivity.this, SettingsActivity.class); 
			//startActivityForResult(startNewActivityOpen, 0); 
		}
		
	}
}

//