package game.party;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class SensorActivity extends Activity implements OnClickListener{
	private static TextView output;
	protected Vibrator vibb;
	private Button recButton, mimicButton;
	private ProgressBar timeBar;
	private SensorManager sensorManager;
	private Sensor sensor;
	private MediaPlayer sound1;
	
	private long startTime;
	private int intervalTime=3; //antalet sekunder som r�relsen skall ta
	
	private static ArrayList<Float> recXList, recYList, recZList;
	private static ArrayList<Float> xList, yList, zList;
	private static ArrayList<Long> recTimeList, timeList;
	
	private static boolean recording=false;
	private static boolean mimic=false;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sensor_layout);
		
		xList=new ArrayList<Float>();
		yList=new ArrayList<Float>();
		zList=new ArrayList<Float>();
		timeList=new ArrayList<Long>();
		
		recXList=new ArrayList<Float>();
		recYList=new ArrayList<Float>();
		recZList=new ArrayList<Float>();
		recTimeList=new ArrayList<Long>();
		
		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensor = sensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);

		recButton=(Button) findViewById(R.id.rec_button);
		recButton.setOnClickListener(this);
		
		mimicButton=(Button)findViewById(R.id.mimic_button);
		mimicButton.setOnClickListener(this);
		
		timeBar=(ProgressBar) findViewById(R.id.time_bar);
		timeBar.setProgress(timeBar.getMax());
	
		output = (TextView) findViewById(R.id.result);
		
		
		vibb=(Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
		

		this.setRequestedOrientation(1);//fixera orienteringen vertikalt 
	}
 
	protected void onResume() {
		super.onResume();
		sound1 = MediaPlayer.create(this, R.raw.swordwhip1);
		sensorManager.registerListener(accelerationListener, sensor,
				SensorManager.SENSOR_DELAY_GAME);
	}
 
	@Override
	protected void onStop() {
		//pause the acc-meter
		sensorManager.unregisterListener(accelerationListener);
		super.onStop();
	}
 
	private SensorEventListener accelerationListener = new SensorEventListener() {
		public void onAccuracyChanged(Sensor sensor, int acc) {
		}
		public void onSensorChanged(SensorEvent event) {
			
			float[] values = event.values;
			float x = values[0];
			float y = values[1];
			float z = values[2];
			if(recording){
				long currTime=System.currentTimeMillis()-startTime;
				xList.add(x);
				yList.add(y);
				zList.add(z);
				timeList.add(currTime);
			}else{
				output.setText("X="+Math.round(x)+" Y="+Math.round(y)+ " Z="+Math.round(z));
			}
			//result.setText("X="+Math.round(x)+" Y="+Math.round(y)+" Z="+Math.round(z));
//			float accelationSquareRoot = (x * x + y * y + z * z)
//					/ (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
//			long actualTime = System.currentTimeMillis();
//			if (accelationSquareRoot >= 8) //
//			{
//				if (actualTime - lastUpdate < 200) {
//					return;
//				}
//				
//				
//				lastUpdate = actualTime;
//				sound1.start();
//            	vibb.vibrate(100);
//            	count++;
//            	//result.setText(count+"");
//				
//			}
		}
	};
	public void onClick(View v) {
		if(v.getId()==recButton.getId()){
			xList=new ArrayList<Float>();
			yList=new ArrayList<Float>();
			zList=new ArrayList<Float>();
			timeList=new ArrayList<Long>();
			
			timeBar.setProgress(timeBar.getMax());
			output.setText("seconds remaining: " + intervalTime);
			CountDownTimer tmp=new CountDownTimer(intervalTime*1000, 100) {
			     public void onTick(long millisUntilFinished) {
			    	 output.setText("seconds remaining: " + millisUntilFinished / 1000+"."+millisUntilFinished/100);
			    	 timeBar.setProgress(timeBar.getProgress()-(timeBar.getMax()/(intervalTime*10)));
			     }

			     public void onFinish() {
			    	 timeBar.setProgress(0);
			    	 recording=false;
			    	 output.setText("done!");
			    	 recXList=xList;
			    	 recYList=yList;
			    	 recZList=zList;
			    	 recTimeList=timeList;
			     }
			 };
			 startTime=System.currentTimeMillis();
			 tmp.start();
			 recording=true;
		}else if(v.getId()==mimicButton.getId()){
			xList=new ArrayList<Float>();
			yList=new ArrayList<Float>();
			zList=new ArrayList<Float>();
			timeList=new ArrayList<Long>();
			
			timeBar.setProgress(timeBar.getMax());
			output.setText("seconds remaining: " + intervalTime);
			CountDownTimer tmp=new CountDownTimer(intervalTime*1000, 100) {
			     public void onTick(long millisUntilFinished) {
			    	 output.setText("seconds remaining: " + millisUntilFinished / 1000+"."+millisUntilFinished/100);
			    	 timeBar.setProgress(timeBar.getProgress()-(timeBar.getMax()/(intervalTime*10)));
			     }

			     public void onFinish() {
			    	 timeBar.setProgress(0);
			    	 recording=false;
			    	 output.setText(evaluate());
			    	
			     }
			     private String evaluate() {
			    	 double totalError=0;
					for(int i=0;i<recTimeList.size();i++){
						long tmp=recTimeList.get(i);
						for(int j=0;j<timeList.size();j++){
							long tmp2=timeList.get(j);
							if(tmp2>=tmp){// time v�rdet �r aktuellt
								float xErr=Math.abs(recXList.get(i)-xList.get(j));
								float yErr=Math.abs(recYList.get(i)-yList.get(j));
								float zErr=Math.abs(recZList.get(i)-zList.get(j));
								double squareError=Math.sqrt(Math.pow(xErr, 2)+Math.pow(yErr, 2)+Math.pow(zErr, 2));
								totalError+=squareError;
							}
						}
					}
					totalError=(totalError/recTimeList.size());
					return totalError+"";
			     }
			 };
			 startTime=System.currentTimeMillis();
			 tmp.start();
			 recording=true;
		}
		
	}
	protected void onPause(){
		sound1.release();
		sensorManager.unregisterListener(accelerationListener);
		super.onPause();
		
	}	

//	
//	new CountDownTimer{ bar = (ProgressBar) findViewById(R.id.progress);
//	    bar.setProgress(total);
//	    int twoMin = 2 * 60 * 1000; // 2 minutes in milli seconds
//
//	    /** CountDownTimer starts with 2 minutes and every onTick is 1 second */
//	    cdt = new CountDownTimer(twoMin, 1000) { 
//
//	        public void onTick(long millisUntilFinished) {
//
//	            total = (int) ((dTotal / 120) * 100);
//	            bar.setProgress(total);
//	        }
//
//	        public void onFinish() {
//	             // DO something when 2 minutes is up
//	        }
//	    }.start();
}

