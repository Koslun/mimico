package game.party;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Copy-pasted PartyStarterActivity code, TODO: change to do something
 * 
 * @author wamai
 * 
 */
public class CountdownActivity extends Activity {
	private Button motionStartButton;
	private ImageView im;
	private Bundle b;
	private TextView mimickedPlayerView, countdownNbrView;
	private static final int COUNTDOWN_LENGTH = 3000; // milliseconds for
														// countdown

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		ViewGroup layout = (ViewGroup) findViewById(R.id.main);
		setLayoutAnimation(layout, this);

		DataStorage dataStorage = ((DataStorage) getApplicationContext());
		String currentPlayer = dataStorage.getCurrentPlayer();
		mimickedPlayerView = (TextView) findViewById(R.id.countdownText);
		mimickedPlayerView.setText(currentPlayer);
		countdownNbrView = (TextView) findViewById(R.id.countdownNbrView);

		CountDownTimer tmp = new CountDownTimer(COUNTDOWN_LENGTH, 100) {
			public void onTick(long millisUntilFinished) {
				long timeLeft = COUNTDOWN_LENGTH - millisUntilFinished;
				timeLeft = timeLeft / 1000; // convert to seconds
				long rest = timeLeft % ((int) timeLeft);
				if (rest <= 150) {
					countdownNbrView.setText((int) timeLeft);
					// TODO: Add sound
				}
			}

			public void onFinish() {
				countdownNbrView.setText("Go!");
				Intent countdown = new Intent(CountdownActivity.this,
						SensorActivity.class);
				// TODO: don't know what below is, need to adjust to make it
				// work (maybe it already does?)
				b = new Bundle();
				b.putInt("movementTime", 5);
				countdown.putExtras(b);
				startActivityForResult(countdown, 0);
			}
		};
	}

	private void setLayoutAnimation(ViewGroup panel, Context ctx) {
		LayoutAnimationController controller = AnimationUtils
				.loadLayoutAnimation(ctx, R.anim.layout_anim_controller);
		panel.setLayoutAnimation(controller);
	}
}