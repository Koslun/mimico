package game.party;

import java.util.ArrayList;

import android.app.Application;

class DataStorage extends Application {

	private String currentPlayer;
	private Integer mimickedIndex, mimickerIndex;
	private ArrayList<String> playerRoster;
	
	public Integer getMimickedIndex(){
		return mimickedIndex;
	}
	
	public Integer incrementMimickedIndex(){
		if(mimickedIndex == null){
			mimickedIndex = 0;
		}
		Integer tmp = mimickedIndex;
		mimickedIndex++;
		return tmp;
	}
	
	public void setMimickedIndex(Integer mimickedIndex){
		if(mimickedIndex == null){
			mimickedIndex = 0;
		}
		this.mimickedIndex = mimickedIndex;
	}
	
	public Integer getMimickerIndex(){
		return mimickerIndex;
	}
	
	public Integer incrementMimickerIndex(){
		if(mimickerIndex == null){
			mimickerIndex = 0;
		}
		Integer tmp = mimickerIndex;
		mimickerIndex++;
		return tmp;
	}
	
	public void setMimickerIndex(Integer mimickerIndex){
		if(mimickerIndex == null){
			mimickerIndex = 0;
		}
		this.mimickerIndex = mimickerIndex;
	}
	
	public ArrayList<String> getPlayerRoster(){
		return playerRoster;
	}
	
	public void addPlayerToRoster(String player){
		if(playerRoster == null){
			playerRoster = new ArrayList<String>();
		} 
		playerRoster.add(player);
	}
	
	public void setPlayerRoster(ArrayList<String> playerRoster){
		this.playerRoster = playerRoster;
	}
	
	public String getCurrentPlayer(){
		return currentPlayer;
	}
	
	public void setCurrentPlayer(String currentPlayer){
		this.currentPlayer = currentPlayer;
	}
}