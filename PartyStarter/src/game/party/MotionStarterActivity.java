package game.party;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Copy-pasted PartyStarterActivity code, TODO: change to do something
 * 
 * @author wamai
 * 
 */
public class MotionStarterActivity extends Activity implements OnClickListener {
	private Button motionStartButton;
	private ImageView im;
	private Bundle b;
	private TextView mimickedPlayerView;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		ViewGroup layout = (ViewGroup) findViewById(R.id.main);
		setLayoutAnimation(layout, this);

		DataStorage dataStorage = ((DataStorage) getApplicationContext());
		String currentPlayer = dataStorage.getCurrentPlayer();
		mimickedPlayerView = (TextView) findViewById(R.id.motionStartText);
		mimickedPlayerView.setText(currentPlayer);

		motionStartButton = (Button) findViewById(R.id.motionStartButton);
		motionStartButton.setOnClickListener(this);
	}

	private void setLayoutAnimation(ViewGroup panel, Context ctx) {
		LayoutAnimationController controller = AnimationUtils
				.loadLayoutAnimation(ctx, R.anim.layout_anim_controller);
		panel.setLayoutAnimation(controller);
	}

	public void onClick(View v) {
		if (v.getId() == R.id.motionStartButton) {
			Intent countdown = new Intent(MotionStarterActivity.this,
					CountdownActivity.class);
			// TODO: don't know what below is, need to adjust to make it work
			// (maybe it already does?)
			b = new Bundle();
			b.putInt("movementTime", 5);
			countdown.putExtras(b);
			startActivityForResult(countdown, 0);
		}
	}
}